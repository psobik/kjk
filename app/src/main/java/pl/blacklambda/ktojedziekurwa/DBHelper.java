package pl.blacklambda.ktojedziekurwa;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

public class DBHelper  extends SQLiteOpenHelper {
    //version number to upgrade database version
    //each time if you Add, Edit table, you need to change the
    //version number.
    private static final int DATABASE_VERSION = 6;

    // Database Name
    private static final String DATABASE_NAME = "przejazd.db";

    public DBHelper(Context context ) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //All necessary tables you like to create will create here

        String CREATE_TABLE_STUDENT = "CREATE TABLE " + Przejazd.TABLE  + "("
                + Przejazd.KEY_data  + " INTEGER,"
                + Przejazd.KEY_tomek + " INTEGER, "
                + Przejazd.KEY_seba + " INTEGER, "
                + Przejazd.KEY_mateusz + " INTEGER, "
                + Przejazd.KEY_karol + " INTEGER, "
                + Przejazd.KEY_piotrek + " INTEGER )";

        db.execSQL(CREATE_TABLE_STUDENT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Przejazd.TABLE);
        onCreate(db);

    }

    public String importDatabase(String dbPath) throws IOException {

        close();
        File newDb = new File(dbPath);

        File oldDb = new File("/data/data/pl.blacklambda.ktojedziekurwa/databases/przejazd.db");
        if (newDb.exists()) {
            copy(newDb,oldDb);
            getWritableDatabase().close();

            return getWritableDatabase().getPath();
        }
        return "";
    }

    private void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static void copyFile(FileInputStream fromFile, FileOutputStream toFile) throws IOException {
        FileChannel fromChannel = null;
        FileChannel toChannel = null;
        try {
            fromChannel = fromFile.getChannel();
            toChannel = toFile.getChannel();
            fromChannel.transferTo(0, fromChannel.size(), toChannel);
        } finally {
            try {
                if (fromChannel != null) {
                    fromChannel.close();
                }
            } finally {
                if (toChannel != null) {
                    toChannel.close();
                }
            }
        }
    }

}