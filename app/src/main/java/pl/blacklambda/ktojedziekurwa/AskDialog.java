package pl.blacklambda.ktojedziekurwa;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import java.util.ArrayList;

public class AskDialog extends DialogFragment {


    private int date;
    private String driver;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int arrayId = getArguments().getInt("arrayId");
        int info = getArguments().getInt("info");
        date = getArguments().getInt("date");
        driver = getArguments().getString("driver");
        final ArrayList<Integer> mSelectedItems = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(info)
                .setMultiChoiceItems(arrayId, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                if (isChecked) {
                                    mSelectedItems.add(which);
                                } else if (mSelectedItems.contains(which)) {
                                    mSelectedItems.remove(Integer.valueOf(which));
                                }
                            }
                        })
                .setPositiveButton(R.string.approve, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        PrzejazdRepo pr = new PrzejazdRepo(getActivity());
                        Przejazd p = new Przejazd();
                        p.date = date;
                        int tomek = 0;
                        int seba = 0;
                        int mateusz = 0;
                        int piotrek = 0;
                        int karol = 0;
                        int iloscPasazerow = mSelectedItems.size();
                        switch (driver) {
                            case "Tomek":
                                tomek = 60 * iloscPasazerow / (iloscPasazerow + 1);
                                if (mSelectedItems.contains(0)) {
                                    seba = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(1)) {
                                    mateusz = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(2)) {
                                    piotrek = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(3)) {
                                    karol = -60 / (iloscPasazerow + 1);
                                }
                                break;
                            case "Seba":
                                seba = 60 * iloscPasazerow / (iloscPasazerow + 1);
                                if (mSelectedItems.contains(0)) {
                                    tomek = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(1)) {
                                    mateusz = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(2)) {
                                    piotrek = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(3)) {
                                    karol = -60 / (iloscPasazerow + 1);
                                }
                                break;
                            case "Mateusz":
                                mateusz = 60 * iloscPasazerow / (iloscPasazerow + 1);
                                if (mSelectedItems.contains(0)) {
                                    tomek = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(1)) {
                                    seba = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(2)) {
                                    piotrek = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(3)) {
                                    karol = -60 / (iloscPasazerow + 1);
                                }
                                break;
                            case "Piotrek":
                                piotrek = 60 * iloscPasazerow / (iloscPasazerow + 1);
                                if (mSelectedItems.contains(0)) {
                                    tomek = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(1)) {
                                    seba = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(2)) {
                                    mateusz = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(3)) {
                                    karol = -60 / (iloscPasazerow + 1);
                                }
                                break;
                            case "Karol":
                                karol = 60 * iloscPasazerow / (iloscPasazerow + 1);
                                if (mSelectedItems.contains(0)) {
                                    tomek = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(1)) {
                                    seba = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(2)) {
                                    mateusz = -60 / (iloscPasazerow + 1);
                                }
                                if (mSelectedItems.contains(3)) {
                                    piotrek = -60 / (iloscPasazerow + 1);
                                }
                                break;
                        }
                        p.seba = seba;
                        p.tomek = tomek;
                        p.mateusz = mateusz;
                        p.piotrek = piotrek;
                        p.karol = karol;
                        pr.insert(p);
                        ((MainActivity) getActivity()).refresh();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        return builder.create();
    }
}