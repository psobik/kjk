package pl.blacklambda.ktojedziekurwa;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

@SuppressLint("SimpleDateFormat")
public class MainActivity extends AppCompatActivity {
    private CaldroidFragment caldroidFragment;

    private LinearLayout tomekLayout;
    private LinearLayout sebaLayout;
    private LinearLayout mateuszLayout;
    private LinearLayout piotrekLayout;
    private LinearLayout karolLayout;
    private ArrayList<HashMap<String, Integer>> map;

    private String m_chosenDir = "";

    private Integer selectedColor = null;
    private static final int PICKFILE_RESULT_CODE = 1;
    private static final int PICKDIR_RESULT_CODE = 2;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        boolean m_newFolderEnabled = true;
        switch (item.getItemId()) {
            case R.id.export_database:
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                    String resultFileName = Environment.getExternalStorageDirectory()+""+"/"+"BazaKJK_"+sdf.format(new Date())+".sqlite";
                    DBHelper.copyFile(new FileInputStream("/data/data/pl.blacklambda.ktojedziekurwa/databases/przejazd.db"), new FileOutputStream(resultFileName));
                    Toast.makeText(MainActivity.this, R.string.datebase_saved+": " +resultFileName, Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.import_database:
                Intent importIntent = new Intent(Intent.ACTION_GET_CONTENT);
                importIntent.setType("*/*");
                importIntent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(
                        Intent.createChooser(importIntent, "Select a File to Upload"),
                        PICKFILE_RESULT_CODE);
                return true;
            case R.id.share_database:
                Intent intentShareFile = new Intent(Intent.ACTION_SEND);
                File outputDir = getApplicationContext().getCacheDir();
                File outputFile = null;
                try {
                    // context being the Activity pointer
                    outputFile = File.createTempFile("BazaKJK", ".sqlite", new File(Environment.getExternalStorageDirectory()+""));
                    DBHelper.copyFile(new FileInputStream("/data/data/pl.blacklambda.ktojedziekurwa/databases/przejazd.db"), new FileOutputStream(outputFile));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (outputFile != null && outputFile.exists()) {
                    intentShareFile.setType("*/*");
                    Toast.makeText(getApplicationContext(),outputFile.getAbsolutePath(),Toast.LENGTH_LONG).show();
                    intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + outputFile.getAbsolutePath()));

                    intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                            outputFile.getName());
                    intentShareFile.putExtra(Intent.EXTRA_TEXT, outputFile.getName());

                    startActivity(Intent.createChooser(intentShareFile, "Udostępnij bazę"));
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    //persmission method.
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have read or write permission
        int writePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        verifyStoragePermissions(this);
        caldroidFragment = new CaldroidFragment();
        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState,
                    "CALDROID_SAVED_STATE");
        } else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putInt(CaldroidFragment.START_DAY_OF_WEEK, CaldroidFragment.MONDAY);
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
            caldroidFragment.setArguments(args);
        }

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();

        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
            }

            @Override
            public void onChangeMonth(int month, int year) {
            }

            @Override
            public void onLongClickDate(final Date date, View view) {
                if (calendarContainDate(date)) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Usuń wpis")
                            .setMessage("Czy na pewno usunąć wpis dla wybranego dnia(" + getAddInfo(date) + ")?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    PrzejazdRepo pr = new PrzejazdRepo(getApplicationContext());
                                    Calendar cal = Calendar.getInstance(); // locale-specific
                                    cal.setTime(date);
                                    cal.set(Calendar.HOUR_OF_DAY, 0);
                                    cal.set(Calendar.MINUTE, 0);
                                    cal.set(Calendar.SECOND, 0);
                                    cal.set(Calendar.MILLISECOND, 0);
                                    long time = cal.getTimeInMillis();
                                    pr.delete((int) (time / 1000));
                                    caldroidFragment.clearBackgroundResourceForDate(new Date(time));
                                    refresh();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                } else if (selectedColor != null) {
                    PrzejazdRepo pr = new PrzejazdRepo(getApplicationContext());
                    Calendar cal = Calendar.getInstance(); // locale-specific
                    cal.setTime(date);
                    cal.set(Calendar.HOUR_OF_DAY, 0);
                    cal.set(Calendar.MINUTE, 0);
                    cal.set(Calendar.SECOND, 0);
                    cal.set(Calendar.MILLISECOND, 0);
                    long time = cal.getTimeInMillis();

                    DialogFragment newFragment = new AskDialog();
                    Bundle bundle = new Bundle();
                    String driver = "";
                    int arrayId = 0;
                    int info = 0;
                    if (selectedColor == R.color.tomek_color) {
                        driver = "Tomek";
                        arrayId = R.array.tomek_array;
                        info = R.string.selectPassengersTomek;
                    } else if (selectedColor == R.color.seba_color) {
                        driver = "Seba";
                        arrayId = R.array.seba_array;
                        info = R.string.selectPassengersSeba;
                    } else if (selectedColor == R.color.mateusz_color) {
                        driver = "Mateusz";
                        arrayId = R.array.mateusz_array;
                        info = R.string.selectPassengersMateusz;
                    } else if (selectedColor == R.color.piotrek_color) {
                        driver = "Piotrek";
                        arrayId = R.array.piotrek_array;
                        info = R.string.selectPassengersPiotrek;
                    } else if (selectedColor == R.color.karol_color) {
                        driver = "Karol";
                        arrayId = R.array.karol_array;
                        info = R.string.selectPassengersKarol;
                    }
                    bundle.putInt("date", (int) (time / 1000));
                    bundle.putInt("arrayId", arrayId);
                    bundle.putInt("info", info);
                    bundle.putString("driver", driver);
                    newFragment.setArguments(bundle);
                    newFragment.show(getFragmentManager(), "missiles");
                }
            }

            @Override
            public void onCaldroidViewCreated() {
            }

        };

        caldroidFragment.setCaldroidListener(listener);

        tomekLayout = (LinearLayout) findViewById(R.id.layout1);
        sebaLayout = (LinearLayout) findViewById(R.id.layout2);
        mateuszLayout = (LinearLayout) findViewById(R.id.layout3);
        piotrekLayout = (LinearLayout) findViewById(R.id.layout4);
        karolLayout = (LinearLayout) findViewById(R.id.layout5);

        tomekLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                sebaLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                mateuszLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                piotrekLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                karolLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                tomekLayout.setBackgroundColor(getResources().getColor(R.color.tomek_color));
                selectedColor = R.color.tomek_color;
            }
        });

        sebaLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                sebaLayout.setBackgroundColor(getResources().getColor(R.color.seba_color));
                mateuszLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                piotrekLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                karolLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                tomekLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                selectedColor = R.color.seba_color;
            }
        });

        mateuszLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                sebaLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                mateuszLayout.setBackgroundColor(getResources().getColor(R.color.mateusz_color));
                piotrekLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                karolLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                tomekLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                selectedColor = R.color.mateusz_color;
            }
        });

        piotrekLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                sebaLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                mateuszLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                karolLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                piotrekLayout.setBackgroundColor(getResources().getColor(R.color.piotrek_color));
                tomekLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                selectedColor = R.color.piotrek_color;
            }
        });

        karolLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                sebaLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                mateuszLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                karolLayout.setBackgroundColor(getResources().getColor(R.color.karol_color));
                piotrekLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                tomekLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                selectedColor = R.color.karol_color;
            }
        });
        refresh();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (caldroidFragment != null) {
            caldroidFragment.saveStatesToKey(outState, "CALDROID_SAVED_STATE");
        }
    }

    public void refresh() {
        PrzejazdRepo pr = new PrzejazdRepo(this);
        map = pr.getPrzejazdList();
        ArrayList<HashMap<String, Integer>> map = pr.getPrzejazdList();
        for (HashMap<String, Integer> item : map) {
            Log.e("test", item.get("date") + "," + item.get("tomek") + "," + item.get("seba") + "," + item.get("mateusz") + "," + item.get("piotrek") + "," + item.get("karol"));
        }
        for (HashMap<String, Integer> item : map) {
            int color = 0;
            if (item.get("tomek") > 0)
                color = R.color.tomek_color;
            else if (item.get("seba") > 0)
                color = R.color.seba_color;
            else if (item.get("mateusz") > 0)
                color = R.color.mateusz_color;
            else if (item.get("piotrek") > 0)
                color = R.color.piotrek_color;
            else if (item.get("karol") > 0)
                color = R.color.karol_color;
            caldroidFragment.setBackgroundResourceForDate(color, new Date(((long) item.get("date")) * 1000L));
        }

        //1447455600
        HashMap<String, Integer> sumMap = pr.getSum();
        int min = Integer.MAX_VALUE;
        for (Integer minSum : sumMap.values()) {
            min = Math.min(min, minSum);
        }
        int tomekSum = sumMap.get("tomek") - min;
        int sebaSum = sumMap.get("seba") - min;
        int mateuszSum = sumMap.get("mateusz") - min;
        int piotrekSum = sumMap.get("piotrek") - min;
        int karolSum = sumMap.get("karol") - min;


        ((TextView) findViewById(R.id.tomekTextView)).setText("T (" + tomekSum + ")");
        if (tomekSum == 0) {
            ((TextView) findViewById(R.id.tomekTextView)).setTypeface(((TextView) findViewById(R.id.tomekTextView)).getTypeface(), Typeface.BOLD_ITALIC);
        } else {
            ((TextView) findViewById(R.id.tomekTextView)).setTypeface(Typeface.DEFAULT);
        }

        ((TextView) findViewById(R.id.sebaTextView)).setText("S (" + sebaSum + ")");
        if (sebaSum == 0) {
            ((TextView) findViewById(R.id.sebaTextView)).setTypeface(((TextView) findViewById(R.id.sebaTextView)).getTypeface(), Typeface.BOLD_ITALIC);
        } else {
            ((TextView) findViewById(R.id.sebaTextView)).setTypeface(Typeface.DEFAULT);
        }
        ((TextView) findViewById(R.id.mateuszTextView)).setText("M (" + mateuszSum + ")");
        if (mateuszSum == 0) {
            ((TextView) findViewById(R.id.mateuszTextView)).setTypeface(((TextView) findViewById(R.id.mateuszTextView)).getTypeface(), Typeface.BOLD_ITALIC);
        } else {
            ((TextView) findViewById(R.id.mateuszTextView)).setTypeface(Typeface.DEFAULT);
        }
        ((TextView) findViewById(R.id.piotrekTextView)).setText("P (" + piotrekSum + ")");
        if (piotrekSum == 0) {
            ((TextView) findViewById(R.id.piotrekTextView)).setTypeface(((TextView) findViewById(R.id.piotrekTextView)).getTypeface(), Typeface.BOLD_ITALIC);
        } else {
            ((TextView) findViewById(R.id.piotrekTextView)).setTypeface(Typeface.DEFAULT);
        }
        ((TextView) findViewById(R.id.karolTextView)).setText("K (" + karolSum + ")");
        if (karolSum == 0) {
            ((TextView) findViewById(R.id.karolTextView)).setTypeface(((TextView) findViewById(R.id.karolTextView)).getTypeface(), Typeface.BOLD_ITALIC);
        } else {
            ((TextView) findViewById(R.id.karolTextView)).setTypeface(Typeface.DEFAULT);
        }
        caldroidFragment.refreshView();
    }

    private boolean calendarContainDate(Date d) {
        Calendar cal = Calendar.getInstance(); // locale-specific
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        int time = (int) (cal.getTimeInMillis() / 1000);
        for (HashMap<String, Integer> item : map) {
            if (item.get("date") == time)
                return true;
        }
        return false;
    }

    private HashMap<String, Integer> getDataByDate(Date d) {
        Calendar cal = Calendar.getInstance(); // locale-specific
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        int time = (int) (cal.getTimeInMillis() / 1000);
        for (HashMap<String, Integer> item : map) {
            if (item.get("date") == time)
                return item;
        }
        return null;
    }

    private String getAddInfo(Date d) {
        HashMap<String, Integer> data = getDataByDate(d);
        String info = "";
        if (data.get("tomek") > 0)
            info += "Kierowca: Tomek, pasażerowie:";
        if (data.get("seba") > 0)
            info += "Kierowca: Seba, pasażerowie:";
        if (data.get("mateusz") > 0)
            info += "Kierowca: Mateusz, pasażerowie:";
        if (data.get("piotrek") > 0)
            info += "Kierowca: Piotrek, pasażerowie:";
        if (data.get("karol") > 0)
            info += "Kierowca: Karol, pasażerowie:";

        if (data.get("tomek") < 0)
            info += " Tomek,";
        if (data.get("seba") < 0)
            info += " Seba,";
        if (data.get("mateusz") < 0)
            info += " Mateusz,";
        if (data.get("piotrek") < 0)
            info += "  Piotrek,";
        if (data.get("karol") < 0)
            info += "  Karol,";
        if (info.length() > 0)
            info = info.substring(0, info.length() - 1);

        return info;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICKFILE_RESULT_CODE:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    String filePath = null;
                    try {
                        filePath = getPath(this, uri);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    if(filePath.startsWith("/file"))
                        filePath = filePath.substring(5);
                    if(filePath.startsWith("/external_files"))
                        filePath = filePath.substring(15);
                    try {
                        DBHelper dbHelper = new DBHelper(getApplicationContext());
                        String location = dbHelper.importDatabase(filePath);
                        Toast.makeText(MainActivity.this, "Pomyślnie zaimportowano plik: " + filePath +" do "+ location, Toast.LENGTH_LONG).show();
                        refresh();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(MainActivity.this, "Wybrano nieprawidłowy plik "+ e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
                break;

        }
    }


    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
}