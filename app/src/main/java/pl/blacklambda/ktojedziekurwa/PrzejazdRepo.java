package pl.blacklambda.ktojedziekurwa;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;

public class PrzejazdRepo {
    private DBHelper dbHelper;

    public PrzejazdRepo(Context context) {
        dbHelper = new DBHelper(context);
    }

    public int insert(Przejazd przejazd) {

        //Open connection to write data
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Przejazd.KEY_data, przejazd.date);
        values.put(Przejazd.KEY_tomek,przejazd.tomek);
        values.put(Przejazd.KEY_seba, przejazd.seba);
        values.put(Przejazd.KEY_mateusz, przejazd.mateusz);
        values.put(Przejazd.KEY_piotrek, przejazd.piotrek);
        values.put(Przejazd.KEY_karol, przejazd.karol);

        // Inserting Row
        long student_Id = db.insert(Przejazd.TABLE, null, values);
        db.close(); // Closing database connection
        return (int) student_Id;
    }

    public void delete(int date) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(Przejazd.TABLE, Przejazd.KEY_data + "= "+date, null);
        db.close(); // Closing database connection
    }

    public void update(Przejazd przejazd) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Przejazd.KEY_tomek, przejazd.tomek);
        values.put(Przejazd.KEY_seba,przejazd.seba);
        values.put(Przejazd.KEY_mateusz, przejazd.mateusz);
        values.put(Przejazd.KEY_piotrek, przejazd.piotrek);
        values.put(Przejazd.KEY_karol, przejazd.karol);

        // It's a good practice to use parameter ?, instead of concatenate string
        db.update(Przejazd.TABLE, values, Przejazd.KEY_data + "= ?", new String[]{String.valueOf(przejazd.date)});
        db.close(); // Closing database connection
    }

    public HashMap<String,Integer> getSum(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        HashMap<String,Integer> resultMap = new HashMap<>();
        Cursor cursor = db.rawQuery("select sum(tomek) as tsum,sum(seba) as ssum, sum(mateusz) as msum,sum(piotrek) as psum,sum(karol) as ksum from przejazd", null);
        // looping through all rows and adding to list

        if (cursor.moveToFirst()) {
            do {
                resultMap.put("tomek", cursor.getInt(cursor.getColumnIndex("tsum")));
                resultMap.put("seba", cursor.getInt(cursor.getColumnIndex("ssum")));
                resultMap.put("mateusz", cursor.getInt(cursor.getColumnIndex("msum")));
                resultMap.put("piotrek", cursor.getInt(cursor.getColumnIndex("psum")));
                resultMap.put("karol", cursor.getInt(cursor.getColumnIndex("ksum")));

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return resultMap;
    }

    public ArrayList<HashMap<String, Integer>>  getPrzejazdList() {
        //Open connection to read only
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery =  "SELECT  " +
                Przejazd.KEY_data + "," +
                Przejazd.KEY_tomek + "," +
                Przejazd.KEY_seba + "," +
                Przejazd.KEY_mateusz + "," +
                Przejazd.KEY_piotrek + "," +
                Przejazd.KEY_karol +
                " FROM " + Przejazd.TABLE;

        //Student student = new Student();
        ArrayList<HashMap<String, Integer>> przejazdList = new ArrayList<>();

        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, Integer> przejazd = new HashMap<String, Integer>();
                przejazd.put("date", cursor.getInt(cursor.getColumnIndex(Przejazd.KEY_data)));
                przejazd.put("tomek", cursor.getInt(cursor.getColumnIndex(Przejazd.KEY_tomek)));
                przejazd.put("seba", cursor.getInt(cursor.getColumnIndex(Przejazd.KEY_seba)));
                przejazd.put("mateusz", cursor.getInt(cursor.getColumnIndex(Przejazd.KEY_mateusz)));
                przejazd.put("piotrek", cursor.getInt(cursor.getColumnIndex(Przejazd.KEY_piotrek)));
                przejazd.put("karol", cursor.getInt(cursor.getColumnIndex(Przejazd.KEY_karol)));
                przejazdList.add(przejazd);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return przejazdList;

    }

    public Przejazd getStudentByDate(int date){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery =  "SELECT  " +
                Przejazd.KEY_data + "," +
                Przejazd.KEY_tomek + "," +
                Przejazd.KEY_seba + "," +
                Przejazd.KEY_mateusz + "," +
                Przejazd.KEY_piotrek + "," +
                Przejazd.KEY_karol +
                " FROM " + Przejazd.TABLE
                + " WHERE " +
                Przejazd.KEY_data + "=?";// It's a good practice to use parameter ?, instead of concatenate string

        Przejazd przejazd = new Przejazd();

        Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(date) } );

        if (cursor.moveToFirst()) {
            do {
                przejazd.date =cursor.getInt(cursor.getColumnIndex(Przejazd.KEY_data));
                przejazd.tomek =cursor.getInt(cursor.getColumnIndex(Przejazd.KEY_tomek));
                przejazd.seba  =cursor.getInt(cursor.getColumnIndex(Przejazd.KEY_seba));
                przejazd.mateusz  =cursor.getInt(cursor.getColumnIndex(Przejazd.KEY_mateusz));
                przejazd.piotrek =cursor.getInt(cursor.getColumnIndex(Przejazd.KEY_piotrek));
                przejazd.karol =cursor.getInt(cursor.getColumnIndex(Przejazd.KEY_karol));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return przejazd;
    }

}