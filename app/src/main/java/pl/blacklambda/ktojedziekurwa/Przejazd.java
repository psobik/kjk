package pl.blacklambda.ktojedziekurwa;



public class Przejazd {

    public static final String TABLE = "Przejazd";

    public static final String KEY_data = "data";
    public static final String KEY_tomek = "tomek";
    public static final String KEY_seba = "seba";
    public static final String KEY_mateusz = "mateusz";
    public static final String KEY_piotrek = "piotrek";
    public static final String KEY_karol = "karol";

    // property help us to keep data
    public int date;
    public int tomek;
    public int seba;
    public int mateusz;
    public int piotrek;
    public int karol;
}